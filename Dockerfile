FROM library/openjdk:8-jre-stretch AS jre

FROM library/python:2-stretch

COPY --from=jre /usr/lib/jvm /usr/lib/jvm
COPY ./files /opt/wwa
COPY ./entrypoint.sh /usr/local/bin/entrypoint.sh

RUN apt-get update && apt-get install -y cron && rm -rf /var/lib/apt/lists/* \
 && chmod +x /usr/local/bin/entrypoint.sh \
 && python -m easy_install -q "/opt/wwa/wwa_app_example-1.0.1508506802-py2.7.egg" \
 && echo "*/5     *    *    *    *    /usr/local/bin/wwa_devops > /var/log/execution.log 2>&1" > /etc/crontab \
 && crontab -u `whoami` /etc/crontab \
 && /etc/init.d/cron restart

ENTRYPOINT ["/usr/local/bin/entrypoint.sh"]
