# WWA_Devops

You can find the source on [Bitbucket](https://bitbucket.org/gonzalesraul/wwa_devops) and the repository information on [Docker Hub](https://hub.docker.com/r/gonzalesraul03/wwa_devops/).

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

What things you need to install the software and how to install them

```
Docker CE (community edition)
```

### Installing

The steps to get docker installed can be found at the official [guide](https://docs.docker.com/engine/installation/)

After installed you can run the following command to get an ephemeral environment running

```
docker run -d --rm --name wwa_devops gonzalesraul03/wwa_devops:1
```

You can also check the execution on the container running the following command:

```
docker logs -f wwa_devops
```

**\** `wwa_devops` is the name of the container, in case any other name is used, please change it accordingly

## Deployment

Additional notes about how to deploy this on a live system:

The command below shows how to get the container running even after host gets restarted:

```
docker run -d --restart always --name wwa_devops gonzalesraul03/wwa_devops:1
```

Notice that we used the `--restart` policy set as `always` to get the container running after restart, and comparing with the ephemeral command, we've removed the `--rm` of the *CLI*

**\** In case you don't want docker running on the host, You can get it running on a virtual machine using `docker-machine`, follow the instructions on the official [guide](https://docs.docker.com/machine/install-machine/).

## Built With

* [Docker](https://www.docker.com/) - Container tool
* [Jenkins](https://jenkins.io) - CI tool

## Authors

* **Raul Gonzales** - *Container with a java process on startup and a python cron job every 5minutes.* - [Changes](https://bitbucket.org/gonzalesraul/wwa_devops/commits/all)

## License

This project is licensed under the GNU GPL License - see the [LICENSE.md](LICENSE.md) file for details